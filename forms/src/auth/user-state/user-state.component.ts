import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { LoggedInStatus } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserLoginService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';

import { AuthConfigManager } from '@ngauth/services';


// User auth state.

@Component({
  selector: 'ngauth-user-state',
  templateUrl: './user-state.component.html',
  styleUrls: ['./user-state.component.css']
})
export class UserStateComponent implements OnInit {

  @Output() onUserStateChecked = new EventEmitter<LoggedInStatus>();

  // @Input("disabled") disabled: boolean = false;
  buttonDisabled: boolean = false;

  userLoggedIn = false;
  chipLabel: string = "";
  iconName: string = "mood";

  private loginService: IUserLoginService;

  constructor(
    private authConfigManager: AuthConfigManager,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    if(isDL()) dl.log("UserStateComponent constructor");

    this.loginService = this.userAuthServiceFactory.getUserLoginService();

  }

  public updateLoginStatus(loggedIn: boolean) {
    if (loggedIn) {
      this.userLoggedIn = true;
    } else {
      this.userLoggedIn = false;
    }
    this._updateChipUI();
  }

  public refreshUI() {
    this._updateChipUI();
  }

  private _updateChipUI() {
    // this.buttonDisabled = this.disabled;
    this.buttonDisabled = !this.authConfigManager.isLoaded;

    if (this.userLoggedIn) {
      this.chipLabel = "Logged In";
      this.iconName = "person_outline";
    } else {
      this.chipLabel = "Not Logged In";
      this.iconName = "person";
    }
  }

  ngOnInit() {
    // this._updateChipUI();
    this._checkAuthStatus();
  }

  // isLoggedIn(message: string, loggedIn: boolean): void {
  //   if (loggedIn) {
  //     this.userLoggedIn = true;
  //   } else {
  //     this.userLoggedIn = false;
  //   }
  //   this._updateChipUI();
  // }

  private _checkAuthStatus() {
    this.loginService.isAuthenticated().subscribe((status: LoggedInStatus) => {
      if (status.loggedIn) {
        this.userLoggedIn = true;
      } else {
        this.userLoggedIn = false;
      }
      this._updateChipUI();
  
      // ???
      this.onUserStateChecked.emit(status);
    });
  }

  public checkLoginStatus() {
    // this._updateChipUI();
    this._checkAuthStatus();
  }


  // TBD. Not being used...
  logInOrOut() {
    if(isDL()) dl.log(">>> log in/out button clicked.");

    if (this.userLoggedIn) {
      // log out.
      this.loginService.logout();
      // ...
    } else {
      // log in.
      // ???
      // let username = '';
      // let password = '';
      // this.loginService.authenticate(username, password);
      // ...
    }

    this._updateChipUI();
  }

}
