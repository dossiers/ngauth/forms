import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult, NewPasswordInfo } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserRegistrationService, IUserLoginService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { DialogBaseComponent } from '../../base/dialog-base-component';


@Component({
  selector: 'ngauth-confirm-registration',
  templateUrl: './confirm-registration.component.html',
  styleUrls: ['./confirm-registration.component.css'],
})
export class ConfirmRegistrationComponent extends DialogBaseComponent implements OnInit, OnDestroy {

  @Output() onRegistrationConfirmed = new EventEmitter<CognitoResult>();

  confirmationCode: string;
  email: string;
  errorMessage: string;
  private sub: any;

  confirmForm: FormGroup;
  errorTips: { [name: string]: string };

  private registrationService: IUserRegistrationService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authRoutes: DefaultAuthRoutes,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    super();
    if(isDL()) dl.log("ConfirmRegistrationComponent constructor");

    this.registrationService = this.userAuthServiceFactory.getUserRegistrationService();

    this._buildForm();
  }

  private _buildForm() {
    this.confirmForm = this.formBuilder.group({
      code: ['', Validators.compose([Validators.minLength(4), Validators.required])],

    });
    this.errorTips = {
      code: "Confirmation code is invalid",
    }
  }
  ngOnChanges() {
    this.confirmForm.reset({
      // tbd: use a data model with init values...
      code: ''
    });
  }
  onSubmit() {
    if(isDL()) dl.log("onSubmit() called.");

    // TBD: save
    this.confirmationCode = this.confirmForm.controls['code'].value;
    this.errorMessage = null;
    this.registrationService.confirmRegistration(this.email, this.confirmationCode).subscribe((cognitoResult: CognitoResult) => {
      if (cognitoResult.message) { // error
        this.errorMessage = cognitoResult.message;
        if(dl.isLoggable()) dl.log("errorMessage: " + this.errorMessage);
      } else { // success
        // //move to the next step
        // if(dl.isLoggable()) dl.log("redirecting");
        // // tbd:
        // this.router.navigate([this.authRoutes.authRouteInfo.home]);
        // // ...
      }

      // tbd:
      this.onRegistrationConfirmed.emit(cognitoResult);
      // ...
    });

    // this.ngOnChanges();  // ???
    this.callCallback('submit');
  }
  revert() // revert or clear?
  {
    if(isDL()) dl.log("revert() called.");
    this.ngOnChanges();
    this.callCallback('revert');
  }
  cancel() // dismiss
  {
    if(isDL()) dl.log("cancel() called.");
    this.ngOnChanges();
    this.callCallback('cancel');
  }



  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['username']) {
        this.email = params['username'];
        if(dl.isLoggable()) dl.log("Params.username = " + this.email);
      } else {
        if(dl.isLoggable()) dl.log("Params.username missing.");
        // Note: this.email can be set aftewards by its parent, for example.
      }
    });
    this.errorMessage = null;
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}





