import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserLoginService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { DialogBaseComponent } from '../../base/dialog-base-component';


@Component({
  selector: 'ngauth-reset-password-step1',
  templateUrl: './reset-password-step1.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordStep1Component extends DialogBaseComponent implements OnInit {

  @Output() onPasswordResetStarted = new EventEmitter<CognitoResult>();
  
  email: string;
  errorMessage: string;

  resetForm1: FormGroup;
  errorTips: {[name:string]: string};

  private loginService: IUserLoginService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    super();
    console.log("ResetPasswordStep1Component constructor");

    this.loginService = this.userAuthServiceFactory.getUserLoginService();

    this._buildForm();
    this.errorMessage = null;
  }

  private _buildForm() {
    this.resetForm1 = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required]) ],
      
    });
    this.errorTips = {
      email: "Email address is invalid",
    }
  }
  ngOnChanges() {
    this.resetForm1.reset({
      // tbd: use a data model with init values...
      email: ''
    });
  }
  onSubmit() {
    console.log("onSubmit() called.");

    // TBD: save
    this.email = this.resetForm1.controls['email'].value;
    this.errorMessage = null;
    this.loginService.startResetPassword(this.email).subscribe((cognitoResult: CognitoResult) => {
      // if (message == null && result == null) { //error
      //   // tbd:
      //   this.router.navigate([this.authRoutes.authRouteInfo.reset, this.email]);
      //   // ...
      // } else { //success
      //   this.errorMessage = message;
      // }

      // tbd:
      let success = !cognitoResult.message;  // What about result???
      cognitoResult.extra = (success) ? this.email : null;
      this.onPasswordResetStarted.emit(cognitoResult);
      // ...
    });

    this.ngOnChanges();
    this.callCallback('submit');
  }
  revert() // revert or clear?
  { 
    console.log("revert() called.");
    this.ngOnChanges(); 
    this.callCallback('revert');
  }
  cancel() // dismiss
  { 
    console.log("cancel() called.");
    this.ngOnChanges(); 
    this.callCallback('cancel');
  }


  ngOnInit(): void {
  }

  // onNext() {
  //   this.errorMessage = null;
  //   this.userService.forgotPassword(this.email, this);
  // }

  // cognitoCallback(message: string, result: any) {
  //   if (message == null && result == null) { //error
  //     // tbd:
  //     this.router.navigate([this.authRoutes.authRouteInfo.reset, this.email]);
  //     // ...
  //   } else { //success
  //     this.errorMessage = message;
  //   }
  // }
}


@Component({
  selector: 'ngauth-reset-password-step2',
  templateUrl: './reset-password-step2.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordStep2Component extends DialogBaseComponent implements OnInit, OnDestroy {

  @Output() onPasswordResetConfirmed = new EventEmitter<CognitoResult>();
  
  // TBD:
  @Input("min-password") minPasswordLength: number;
  
  verificationCode: string;
  email: string;
  password: string;
  errorMessage: string;
  private sub: any;

  resetForm2: FormGroup;
  errorTips: {[name:string]: string};

  private loginService: IUserLoginService;
  
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authRoutes: DefaultAuthRoutes,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    super();
    console.log("ResetPasswordStep2Component constructor");
    console.log("email from the url: " + this.email);
    
    this.loginService = this.userAuthServiceFactory.getUserLoginService();
    
    this._buildForm();
    this.errorMessage = null;
  }

  private _buildForm() {
    this.resetForm2 = this.formBuilder.group({
      code: ['', Validators.compose([Validators.minLength(4), Validators.required]) ],
      newPassword: ['', Validators.compose([Validators.minLength(6), Validators.required]) ],
      
    });
    this.errorTips = {
      code: "Confirmation code is invalid",
      newPassword: "Password is a miminum of 6 characters",
    }
  }
  ngOnChanges() {
    this.resetForm2.reset({
      // tbd: use a data model with init values...
      code: '',
      newPassword: ''
    });
  }
  onSubmit() {
    console.log("onSubmit() called.");

    // TBD: save
    this.verificationCode = this.resetForm2.controls['code'].value;
    this.password = this.resetForm2.controls['newPassword'].value;
    this.errorMessage = null;
    this.loginService.confirmResetPassword(this.email, this.verificationCode, this.password).subscribe((cognitoResult) => {
      if (cognitoResult.message) { //error
        this.errorMessage = cognitoResult.message;
        console.log("errorMessage: " + this.errorMessage);
      } else { //success
        // // tbd:
        // this.router.navigate([this.authRoutes.authRouteInfo.login]);
        // // ...
      }

      // tbd:
      this.onPasswordResetConfirmed.emit(cognitoResult);
      // ...
    });

    // this.ngOnChanges();  // ???
    this.callCallback('submit');
  }
  revert() // revert or clear?
  { 
    console.log("revert() called.");
    this.ngOnChanges(); 
    this.callCallback('revert');
  }
  cancel() // dismiss
  { 
    console.log("cancel() called.");
    this.ngOnChanges(); 
    this.callCallback('cancel');
  }



  // TBD: How to pass a param when this component is a part of Stepper forms????
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.email = params['email'];
      console.log("Param. username = " + this.email);      
    });
    this.errorMessage = null;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


  // onNext() {
  //   this.errorMessage = null;
  //   this.userService.confirmNewPassword(this.email, this.verificationCode, this.password, this);
  // }

  // cognitoCallback(message: string) {
  //   if (message != null) { //error
  //     this.errorMessage = message;
  //     console.log("result: " + this.errorMessage);
  //   } else { //success
  //     // tbd:
  //     this.router.navigate([this.authRoutes.authRouteInfo.login]);
  //     // ...
  //   }
  // }

}
