import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { IUserRegistrationService } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { DialogBaseComponent } from '../../base/dialog-base-component';


@Component({
  selector: 'ngauth-resend-code',
  templateUrl: './resend-code.component.html',
  styleUrls: ['./resend-code.component.css'],
})
export class ResendCodeComponent extends DialogBaseComponent implements OnInit {

  @Output() onResendCodeSubmitted = new EventEmitter<CognitoResult>();
  
  email: string;
  errorMessage: string;

  resendForm: FormGroup;
  errorTips: {[name:string]: string};

  private registrationService: IUserRegistrationService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private userAuthServiceFactory: UserAuthServiceFactory
  ) {
    super();
    if(isDL()) dl.log("ResendCodeComponent constructor");

    this.registrationService = this.userAuthServiceFactory.getUserRegistrationService();

    this._buildForm();
  }

  private _buildForm() {
    this.resendForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required]) ],
      
    });
    this.errorTips = {
      email: "Email address is invalid",
    }
  }
  ngOnChanges() {
    this.resendForm.reset({
      // tbd: use a data model with init values...
      email: ''
    });
  }
  onSubmit() {
    if(isDL()) dl.log("onSubmit() called.");

    // TBD: save
    this.email = this.resendForm.controls['email'].value;
    this.errorMessage = null;
    this.registrationService.resendCode(this.email).subscribe((cognitoResult: CognitoResult) => {
      // ???
      if (cognitoResult.message) {
        // this.errorMessage = "Something went wrong...please try again";
        this.errorMessage = cognitoResult.message;
        if(dl.isLoggable()) dl.log("errorMessage: " + this.errorMessage);
      } else {
        // // tbd:
        // this.router.navigate([this.authRoutes.authRouteInfo.confirm, this.email]);
        // // ...
      }
  
      // tbd:
      this.onResendCodeSubmitted.emit(cognitoResult);
      // ...
    });

    // this.ngOnChanges();  // ???
    this.callCallback('submit');
  }
  revert() // revert or clear?
  { 
    if(isDL()) dl.log("revert() called.");
    this.ngOnChanges(); 
    this.callCallback('revert');
  }
  cancel() // dismiss
  { 
    if(isDL()) dl.log("cancel() called.");
    this.ngOnChanges(); 
    this.callCallback('cancel');
  }



  ngOnInit(): void {
  }


  // resendCode() {
  //   this.registrationService.resendCode(this.email, this);
  // }

  // cognitoCallback(error: any, result: any) {
  //   if (error != null) {
  //     this.errorMessage = "Something went wrong...please try again";
  //   } else {
  //     // tbd:
  //     this.router.navigate([this.authRoutes.authRouteInfo.confirm, this.email]);
  //     // ...
  //   }
  // }
}