import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthReadyComponent } from './auth-ready.component';

describe('AuthReadyComponent', () => {
  let component: AuthReadyComponent;
  let fixture: ComponentFixture<AuthReadyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthReadyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthReadyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
