import { Component } from "@angular/core";
import { DialogParentCallback } from '@ngauth/core';

// TBD:
// Just use EventEmitter instead of callbacks????

// Components which can be used in a dialog should extend from this class,
// or, implement something similar on its own.
// @Component({
//   selector: 'ngauth-diallog-base',
//   template: ''
// })
export abstract class DialogBaseComponent {

  // callback for dialog related event.
  private callback: (DialogParentCallback | null);

  public setCallback(callback: DialogParentCallback) {
    this.callback = callback;
  }

  // Note that this public (not just protected).
  public callCallback(action: (string | null) = null) {
    if (this.callback) {
      this.callback.dialogOperationDone(action);
    }
  }

}
